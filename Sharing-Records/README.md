# 分享记录说明
每一次的分享都会按Day-年-月-日-时间的形式创建对应的目录，目录中会存放分享的文档材料，在线文档会提供在线文档链接。
每一次的分享视频，离线下载可以在网盘中获取。在线视频，每个分享人自行安排，如果有会在文档中填写在线视频地址。


## 在线文档
每一期分享的视频和文档都会在下面的gitee项目下记录，在项目的Sharing-Records目录下，按日期时间命名。

[Cloud Native Sharing/cloud-native-sharing - Gitee.com](https://gitee.com/cloud-native-sharing/cloud-native-sharing/tree/master/Sharing-Records)

## 在线视频

> B站

每期地址不固定，详情请看每期的分享说明

https://www.bilibili.com/video/xxxx

## 离线网盘下载
每一期分享的视频和文档都会在下面的三种网盘中提供下载。

> 百度网盘

链接: https://pan.baidu.com/s/1yedDXG8C61tX1UON-oT_iA 提取码: obh2

> 天翼云盘

https://cloud.189.cn/t/A3IZNn3A3qqu（访问码：8q9s）

> 阿里云盘

https://www.aliyundrive.com/s/x6bUaLuLQd1 提取码: v83f

没有阿里云盘又想使用的可以点击下面的链接注册，顺便我也涨点容量，用于存储每次的分享视频

```yaml
我在使用超好用的「阿里云盘」，注册就领 300 GB 容量，完成新手任务再领 500 GB，快来试试吧 🎉

------------
点此链接领取福利：
https://pages.aliyundrive.com/mobile-page/web/beinvited.html?code=05ee515
```

## 版权声明

每期分享视频和文档版权归属于分享人，欢迎收藏转载，但请标明出处，感谢！