# cloud-native-sharing

## 介绍
云原生技术分享群分享内容整理

## 初衷
将自己学到的东西写出来、讲出来、分享出来，锻炼自己的学习能力、写作能力、讲解能力。
寻找一些志同道合的道友互相学习、共同进步。

## 分享计划

### 在线文档

有计划分享知识的朋友可以在下面的在线文档中填写相关分享信息，或是与我联系。

https://docs.qq.com/sheet/DZmJSSWlFYmRMbm1K?tab=BB08J2

### 分享群二维码

有计划分享知识的道友可以带着你的分享计划加群

![](https://gitee.com/cloud-native-sharing/cloud-native-sharing/raw/master/grcode.jpeg)
