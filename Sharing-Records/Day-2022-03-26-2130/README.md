# 基于KubeSphere的Kubernetes生产实践之路-起步篇

## 分享日期

2022-03-26 21:30

## 分享人

老Z

## 分享主题

基于KubeSphere的Kubernetes生产实践之路-起步篇

[Kubernetes-on-KubeSphere/基于KubeSphere的Kubernetes生产实践之路-起步篇.md · devops/cloudnative - Gitee.com](https://gitee.com/zdevops/cloudnative/blob/main/Kubernetes-on-KubeSphere/基于KubeSphere的Kubernetes生产实践之路-起步篇.md)

## 分享内容获取方式

### 在线文档

[Cloud Native Sharing/cloud-native-sharing - Gitee.com](https://gitee.com/cloud-native-sharing/cloud-native-sharing/tree/master)
