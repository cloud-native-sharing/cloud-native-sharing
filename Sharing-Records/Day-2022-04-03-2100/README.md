# 基于KubeSphere玩转k8s-GlusterFS安装手记

## 分享日期

2022-04-03 21:00

## 分享人

老Z

## 分享主题

## 在线文档

> 老Z 每期分享的文档都可以在以下链接找到

https://gitee.com/zdevops/z-notes


> 本期分享文档直达链接

[基于KubeSphere玩转k8s-GlusterFS安装手记](https://gitee.com/zdevops/z-notes/blob/main/k8s-on-kubesphere/%E5%9F%BA%E4%BA%8EKubeSphere%E7%8E%A9%E8%BD%ACk8s-GlusterFS%E5%AE%89%E8%A3%85%E6%89%8B%E8%AE%B0.md)


## 在线视频

> B站

本期分享时间比较长，因此视频录了两部分，一部分文档讲解，一部分实操演示。

文档讲解：https://www.bilibili.com/video/BV1CY411J7hd/
实操演示：https://www.bilibili.com/video/BV1oY4y1p7Tw/